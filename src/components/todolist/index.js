import ng from 'angular';

import todoListComponent from './todo-list.component';

export default ng.module('app.components.todolist', [])
    .component('todoList', todoListComponent)
    .name;