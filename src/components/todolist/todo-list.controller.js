export default class TodoListController {
    $onInit() {
        this.title = 'ToDo List';
        this.isShow = true;
        this.todos = [
            { id: 0, title: 'Buy eggs', isComplete: false },
            { id: 1, title: 'Read a book', isComplete: false },
            { id: 2, title: 'Hit the gim', isComplete: false },
            { id: 3, title: 'Conference', isComplete: false },
            { id: 4, title: 'Sleep', isComplete: false },
        ];
    }
    
    fixMe(e) {
        this.title = 'ToDo List';
    };

    onBlur(e) {
    };

    addToDo(e) {
        e.preventDefault();
        let idList = this.todos.map(function(item) {
            return item.id;
        });        
        idList.sort( (a,b) => a-b );
        let newId = idList[idList.length - 1] + 1;
        this.todos.push({ id: newId, title: this.value, isComplete: false });
        let flag = this.todos.map(function(item) {
            return item.id;
        });
        this.value = '';
    };

    remove(e, id) {
        e.preventDefault();
        const index = this.todos.findIndex(function(item) {
            return item.id === id;
        });
        this.todos.splice(index, 1);
    };

    save(e, todo) {
        delete todo.$edit;
    };

    delete() {
        this.onDelete({ id: this.item.id });
    };
};