import template from './todo-list.html';
import controller from './todo-list.controller';

export default {
    template,
    controller,
    bindings: {
        item: '<',
        onDelete: '&'
    }
};