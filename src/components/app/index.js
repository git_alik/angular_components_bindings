import ng from 'angular';

import AppComponent from './app.component';

export default ng.module('app.components.todoitems', [])
    .component('todoMainWrp', AppComponent)
    .name;