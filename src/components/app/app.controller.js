export default class ItemsController {
    $onInit() {
        this.listItems = [
            { id: 0, title: 'ToDo List'},
        ];
    }

    addNewList(e) {
        if (this.listItems.length < 3) {
            let idList = this.listItems.map(function(item) {
                return item.id;
            });
            let newId = idList.sort( (a,b) => a-b )[idList.length - 1] + 1;
            this.listItems.push({ id: newId, title: 'ToDo List' });
        }       
    };

    removeList(id) {
        const index = this.listItems.findIndex(function(item, i, arr) {
            return item.id === id;
        });
        this.listItems.splice(index, 1);
    };
        
};
