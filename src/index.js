import todoList from './components/todolist';
import todoMainWrp from './components/app';
import 'bootstrap/dist/css/bootstrap.min.css';

angular.module('app', [todoMainWrp, todoList]);